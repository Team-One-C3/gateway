//Debe permitirme conectarme con mi microservicio de autenticacion

const {RESTDataSource } = require('apollo-datasource-rest');  //una fuente de datos tipo rest
const {pedido_ms_url} = require('../server');

class PedidoAPI extends RESTDataSource{
  constructor(){
    super();
    this.baseURL = pedido_ms_url
  }
// como esta dentro de una clase, javascript infiere que es una funcion, evitando poner function alluser
  
  allOrders(){
    return this.get('/pedidos/');
  }

  createOrder(product){
    return this.post('/pedidos/',product)
  }

  searchByNumOrden(n){
    return this.get(`/pedidos/${n}`)
  }

}

module.exports = PedidoAPI;  //Me permite hacer global la clase AuthAPI