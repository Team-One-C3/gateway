//Permite conectar con el microservicio de auth

const {RESTDataSource } = require('apollo-datasource-rest');  //una fuente de datos tipo rest
const {auth_ms_url} = require('../server');

class AuthAPI extends RESTDataSource{
  constructor(){
    super();
    this.baseURL = auth_ms_url
  }
   // se debe crear un método para cada uno de los endpoint creados en el backend,
   //es decir para cada una de las vistas expuestas en los microservicios creados en el backend.
   //se deben relacionar los nombres de los métodos con los nombres de las vistas del backend, para tener un orden.
   login(credentials){
    return this.post('/login/', credentials);
  }

  createUser(user){
    return this.post('/user/', user);
  }
  
  getUserDetail(id){
    return this.get(`/user/${id}`, {}, {
      headers:{
          'Authorization': `${this.context.token}`
      }
    });
  }

  refreshToken(refreshToken) {
    return this.post('/refresh/', {
        refresh: refreshToken
    });
  }
}

module.exports = AuthAPI;  //Me permite hacer global la clase AuthAPI