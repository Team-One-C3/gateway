//Archivo princpal del proyecto

const { ApolloServer} = require('apollo-server');
const { authentication} = require('./utils/authentication');
const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');
const AuthAPI = require('./dataSources/auth_ms');
const PedidoAPI = require('./dataSources/pedido_ms');

const server = new ApolloServer(
  {
    typeDefs,
    resolvers,
    dataSources: () => {
      return {
        authAPI: new AuthAPI(),
        pedidoAPI: new PedidoAPI()
      }
    },
    introspection: true,
    playground: true,
    context: authentication
  }
);

// Levantando el servidor
server.listen(process.env.PORT||4000).then((params) => {
  //console.log(params);
  console.log('servidor corriendo');
}).catch(() => {
  console.log('error');
});