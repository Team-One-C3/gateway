const {ApolloError} = require('apollo-server');
const {auth_ms_url} = require('../server');
const fetch = require('node-fetch');

const authentication = async (params) => {
    const {req} = params;
    // console.log(req.headers.authorization)
    const token = req.headers.authorization || '';
    if (token === '') {
        // No mandaron token
        return {};
    } else {
        // Mandaron un token
        try {
            const response = await fetch(`${auth_ms_url}/nuevo-check/`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `${token}`
                }
            });
            // console.log(response , "responseeeee")
            if (response.status === 200) {
                console.log("el status es 200")
                const {id, username} = (await response.json())
                return {
                    id,
                    username,
                    token
                }

            } else {
                throw new ApolloError(`Usuario inactivo`, 400);
            }
        } catch (e) {
            //Capturar si la promesa (fetch) me genero un error (entro al metodo catch)
            throw new ApolloError(`Se genero un error ${e}`, 400);
        }
    }
}

module.exports = authentication;