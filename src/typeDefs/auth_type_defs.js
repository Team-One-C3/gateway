const { gql } = require('apollo-server');

const authTypeDefs = gql`
  input CredentialsInput {
    username: String!
    password: String!
  }

  input CreateUserInput {
    username: String!
    name: String!
    email: String!
    phone: String!
    password: String!
  }

  type Tokens {
    access: String!
    refresh: String!
  }

  type informationUser{
    id: Int!
    username: String!
    name: String!
    email: String!
    phone: String!
  }  

  type AccessToken {
    access: String!
  }

  type usuario {
    estado: String!
    tokenSerializer: Tokens!
  }

  type Mutation {
    logIn(credentials: CredentialsInput!): Tokens!
    refreshToken(refresh: String!): AccessToken!
    createUser(info: CreateUserInput!): informationUser!
    usuarioDetalleById(id: Int!): informationUser! 
  }

  type Query {
    userDetailById: informationUser!
  }
`;

// module.exports = lodash.merge(typeDefs)
module.exports = authTypeDefs
