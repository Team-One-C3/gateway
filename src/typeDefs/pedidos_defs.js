const { gql } = require('apollo-server');

const pedidosTypeDefs = gql`

  input OrderInput {
    tipoProducto: String!
    username: String!
    ciudad: String!
    cantidad: Int!
    precio: Int!
    descripcion: String!
    celular: String!
    direccion: String!
    fecha: String!
  }

  type Order {
    numeroOrden: String!
    tipoProducto: String!
    username: String!
    ciudad: String!
    cantidad: Int!
    precio: Int!
    descripcion: String!
    celular: String!
    direccion: String!
    fecha: String!
  }

  type ReciboOrden {
    id: String!
    numeroOrden: String!
    tipoProducto: String!
    username: String!
    ciudad: String!
    cantidad: Int!
    precio: Int!
    descripcion: String!
    celular: String!
    direccion: String!
    fecha: String!
  }


  extend type Mutation {
    createOrder(product: OrderInput!): ReciboOrden!
    searchOrder(n: String!): Order!
  }

  extend type Query {
    getAllOrder: [Order]!
  }
`;

module.exports = pedidosTypeDefs

