const authResolver = { 
  Query: {
    userDetailById: (_, {} , {dataSources, id}) =>{
      // console.log(dataSources)
      // console.log(id)
      if(id === null) {
        console.log(id, "resolvers - null")
        // No esta autorizado
        throw new ApolloError('No autorizado', 401);
      }
      // console.log(id, "resolvers - debe llevar algo el id")
      return dataSources.authAPI.getUserDetail(id);
    }
  },
  Mutation: {
    logIn: (_, {credentials}, {dataSources}) => {
        return dataSources.authAPI.login(credentials);
    },
    refreshToken: (_, {refresh}, {dataSources}) =>{
      return dataSources.authAPI.refreshToken(refresh);
    },
    createUser: (_, {info} , {dataSources}) =>{
      return dataSources.authAPI.createUser(info);
    },
    usuarioDetalleById: (_, {id} , {dataSources}) =>{
      return dataSources.authAPI.getUserDetail(id);
    }
  }
}

module.exports =  authResolver;
