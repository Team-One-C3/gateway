const lodash = require('lodash');

const authResolver = require('./auth_resolver');
const pedidosResolver = require('./pedidos_resovler');
const resolvers = lodash.merge(authResolver,pedidosResolver);

module.exports = resolvers