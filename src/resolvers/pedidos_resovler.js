const pedidosResolver = { 
  Query: {
    getAllOrder: (_, {} , {dataSources}) => {
      return dataSources.pedidoAPI.allOrders();
    }
  },
  Mutation: {
    createOrder: (_, {product}, {dataSources}) => {
        return dataSources.pedidoAPI.createOrder(product);
    },
    searchOrder: (_ , {n} , {dataSources}) => {
      return dataSources.pedidoAPI.searchByNumOrden(n);
    }
  }
}

module.exports =  pedidosResolver;
